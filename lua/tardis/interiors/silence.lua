--- silence

local function SonicModelExists()
    if SonicSD and file.Exists("models/weapons/c_sonicsd.mdl", "GAME") then
        return true
    end
    return false
end

local T = {}

T.Versions = {
    randomize = false,
    allow_custom = true,
    randomize_custom = false,

    main = {
        id = "silence",
    },
    other = {
		{
			name = "Lodger Door",
			id = "silencelodge",
		},
        {
			name = "Classic Door",
			id = "silencecl",
		},
	}
}

T.Base = "base"
T.Name = "The Silence's Time-Ship"
T.ID = "silence"
T.Interior = {
    Model = "models/FuzzyLeo/Silence/wall.mdl",
    IdleSound = {
        {
            path = "CoolyDude/silence_test_new_hum_seamless.wav",
            volume = 50
        },
    },
    ScreensEnabled = false,

    -- Rotor light handled by templates

    Lamps = {
        platlamp1 = {
            color = Color(255, 11, 11),
            texture = "effects/flashlight/soft",
            fov = 56,
            distance = 100,
            brightness = 0.5,
            pos = Vector(0, 255., 62.5),
            ang = Angle(90, 90, 180),
            shadows = false,
            warn = {
                -- same list of options available
                sprite_brightness = 0.5,
                color = Color(255, 60, 60),
            },
        },
        platlamp2 = {
            color = Color(255, 11, 11),
            texture = "effects/flashlight/soft",
            fov = 56,
            distance = 100,
            brightness = 0.5,
            pos = Vector(0, -255., 62.5),
            ang = Angle(90, 90, 180),
            shadows = false,
            warn = {
                -- same list of options available
                sprite_brightness = 0.5,
                color = Color(255, 60, 60),
            },
        },
        platlamp3 = {
            color = Color(255, 11, 11),
            texture = "effects/flashlight/soft",
            fov = 56,
            distance = 100,
            brightness = 0.5,
            pos = Vector(255, 0., 62.5),
            ang = Angle(90, 90, 180),
            shadows = false,
            warn = {
                -- same list of options available
                sprite_brightness = 0.5,
                color = Color(255, 60, 60),
            },
        },
        platlamp4 = {
            color = Color(255, 11, 11),
            texture = "effects/flashlight/soft",
            fov = 56,
            distance = 100,
            brightness = 0.5,
            pos = Vector(-255, 0., 62.5),
            ang = Angle(90, 90, 180),
            shadows = false,
            warn = {
                -- same list of options available
                sprite_brightness = 0.5,
                color = Color(255, 60, 60),
            },
        },
    },
    Portal = {
        -- Generated by portals debug tool
        pos = Vector(0, 491.95, 53.2),
        ang = Angle(-0, -90, 0),
        width = 85,
        height = 127.5,
        inverted = 0,
        thickness = -6
    },
    Fallback = {
        pos = Vector(0, 468, 6),
        ang = Angle(0, 0, 0)

    },
    Screens = {
        {
            pos = Vector(-6.4, -30, 84),
            ang = Angle(0, 0, 90),
            width = 205,
            height = 200,
            gui_rows = 4,
            power_off_black = false,
        }
    },
    Scanners = {
        {
            mat = "models/fuzzyleo/silence/grilldoor",
            width = 1000,
            height = 450,
            ang = Angle(0,0,0),
            fov = 90,
        }
    },
    ExitDistance = 1000,
    Sounds = {
        Teleport = { },
        Cloister = "ambient/alarms/combine_bank_alarm_loop4.wav",
    },
    Sequences = "silence_sequence",
    Sounds={
		Door={
			enabled=true,
			open = "doors/metal_stop1.wav",
			close = "doors/garage_stop1.wav",
		},
	},
    Parts = {
		door={
            Model="models/FuzzyLeo/Silence/Exteriordoor.mdl",
			posoffset=Vector(35.4, 0, -50.2),
		},
        silence_floor            =   {},
        silence_pillar            =   {},
        silence_rotor            =   {},
        silence_rotortop            =   {},
        silence_control            =   {},
        silence_doorframe            =   {},
        silence_pillar2            =   {},
        silence_floorlight            =   {},
        silence_doorhitbox            =   {pos = Vector(29, 485, 55),                     ang = Angle(0, 0, 0),               },
        silence_demat            =   {pos = Vector(0, -38, 64),                     ang = Angle(0, 0, 0),               },
        silence_flight            =   {pos = Vector(-6, -45, 52),                     ang = Angle(0, 0, 0),               },
        silence_physlock            =   {pos = Vector(6, -45, 52),                     ang = Angle(0, 0, 0),               },
        silence_cloak            =   {pos = Vector(6, 45, 52),                     ang = Angle(0, 0, 0),               },
        silence_float            =   {pos = Vector(-6, 45, 52),                     ang = Angle(0, 0, 0),               },
        silence_enginerelease            =   {pos = Vector(-6, -51, 44),                     ang = Angle(0, 0, 0),               },
        silence_power            =   {pos = Vector(51, 6, 44),                     ang = Angle(0, 0, 0),               },
        silence_manualcoords            =   {pos = Vector(38, 0, 64),                     ang = Angle(0, 0, 0),               },
        silence_coords            =   {pos = Vector(-38, 0, 64),                     ang = Angle(0, 0, 0),               },
        silence_hads            =   {pos = Vector(51, -6, 44),                     ang = Angle(0, 0, 0),               },
        silence_repair            =   {pos = Vector(45, -6, 52),                     ang = Angle(0, 0, 0),               },
        silence_redecorate            =   {pos = Vector(45, 6, 52),                     ang = Angle(0, 0, 0),               },
        silence_handbrake            =   {pos = Vector(6, -51, 44),                     ang = Angle(0, 0, 0),               },
        silence_manualflight            =   {pos = Vector(0, 38, 64),                     ang = Angle(0, 0, 0),               },
        silence_radio            =   {pos = Vector(-51, 6, 44),                     ang = Angle(0, 0, 0),               },
        silence_fastreturn            =   {pos = Vector(6, 51, 44),                     ang = Angle(0, 0, 0),               },
        silence_isomorphic            =   {pos = Vector(-6, 51, 44),                     ang = Angle(0, 0, 0),               },
        silence_doorlock            =   {pos = Vector(-45, 6, 52),                     ang = Angle(0, 0, 0),               },
        silence_doorhitbox2            =   {pos = Vector(-45, -6, 52),                     ang = Angle(0, 0, 0),               },
        silence_scanner            =   {pos = Vector(-51, -6, 44),                     ang = Angle(0, 0, 0),               },
        silence_screen            =   {pos = Vector(0, -30, 67),                     ang = Angle(0, 0, 0),               },
    },
    IntDoorAnimationTime = 0.7,
    Controls = {
        silence_doorhitbox          = "door",
        silence_doorhitbox2          = "door",
        silence_demat            = "teleport_double",
        silence_flight         = "flight",
        default_screen              = nil,
        silence_screen        = "toggle_screens",
        silence_scanner       = "toggle_scanners",
        silence_doorlock         = "doorlock",
        silence_enginerelease       = "engine_release",
        silence_power          = "power",
        silence_manualcoords            = "destination",
        silence_hads                = "hads",
        silence_coords          = "coordinates",
        silence_repair         = "repair",
        silence_redecorate    = "redecorate",
        silence_handbrake           = "handbrake",
        silence_radio          = "music",
        silence_fastreturn            = "fastreturn",
        silence_physlock            = "physlock",
        silence_isomorphic          = "isomorphic",
        silence_float               = "float",
        silence_cloak         = "cloak",
        default_dematcircuit        = nil,
        Empty      = "sonic_dispenser",
        default_sonic_inserted      = SonicModelExists() and "sonic_dispenser",
        silence_manualflight              = "thirdperson",

    },
    Tips = {},
    -- Interior.Tips are deprecated; should be deleted when the extensions update and
    -- replace with Interior.CustomTips, Interior.PartTips and Interior.TipSettings
    TipSettings = {
        view_range_min = 70,
        view_range_max = 90,
    },
    CustomTips = {
        --{ text = "Example", pos = Vector(0, 0, 0) },
    },
    PartTips = {

        silence_doorhitbox            =   {pos = Vector(29, 485, 55), down = true},
        silence_float            =   {pos = Vector(-6, 45, 52), down = true},
        silence_demat            =   {pos = Vector(0, -38, 64), down = true},
        silence_flight            =   {pos = Vector(-6, -45, 52), down = true},
        silence_physlock            =   {pos = Vector(6, -45, 52), down = true},
        silence_screen            =   {pos = Vector(0, -32, 70), down = true},
        silence_scanner            =   {pos = Vector(-51, -3.5, 44), down = true},
        silence_cloak            =   {pos = Vector(6, 45, 52), down = true},
        silence_enginerelease            =   {pos = Vector(-3.5, -51, 44), down = true},
        silence_power            =   {pos = Vector(51, 3.5, 44), down = true},
        silence_manualcoords            =   {pos = Vector(38, 0, 64), down = true},
        silence_hads            =   {pos = Vector(51, -3.5, 44), down = true},
        silence_coords            =   {pos = Vector(-38, 0, 64), down = true},
        silence_repair            =   {pos = Vector(45, -6, 52), down = true},
        silence_redecorate            =   {pos = Vector(45, 6, 52), down = true},
        silence_handbrake            =   {pos = Vector(3.5, -51, 44), down = true},
        silence_radio            =   {pos = Vector(-51, 3.5, 44), down = true},
        silence_fastreturn            =   {pos = Vector(3.5, 51, 44), down = true},
        silence_isomorphic            =   {pos = Vector(-3.5, 51, 44), down = true},
        silence_doorlock            =   {pos = Vector(-45, 6, 52), down = true},
        silence_doorhitbox2            =   {pos = Vector(-45, -6, 52), down = true},
        silence_manualflight            =   {pos = Vector(0, 38, 64), down = true},
    },
    LightOverride = {
        basebrightness = 0,
        nopowerbrightness = 0.0025
    },
    Seats = {
        {
            pos = Vector(130, -96, -30),
            ang = Angle(0, 40, 0)
        },
        {
            pos = Vector(125, 55, -30),
            ang = Angle(0, 135, 0)
        }
    },
    BreakdownEffectPos = Vector(0, 0, 40),
}

T.Exterior = {
    Model="models/FuzzyLeo/Silence/Exteriorframe.mdl",
    DoorAnimationTime = 0.5,
    Parts = {
        door = {
            Model="models/FuzzyLeo/Silence/Exteriordoor.mdl",
            posoffset=Vector(-35.4, 0, -50.2)
        },
        vortex = {
            model = "models/doctorwho1200/copper/2010timevortex.mdl",
            pos = Vector(0,0,50),
            ang = Angle(0,0,0),
            scale =10
        },
    },
    Fallback = {
        pos = Vector(60,0,1),
        ang = Angle(0,0,0)
    },
    ScannerOffset = Vector(40,0,50),
    Sounds = {
        Teleport = {
            demat = "CoolyDude/silence_takeoff.mp3",
            mat = "CoolyDude/silence_landing_new.mp3",
            mat_fast = "ambient/machines/teleport3.wav",
            mat_damaged_fast = "p00gie/tardis/mat_damaged_fast.wav",
            fullflight = "ambient/machines/teleport3.wav",
            interrupt = "drmatt/tardis/repairfinish.wav",
        },

        Door={
			enabled=true,
			open="doors/metal_stop1.wav",
			close="doors/garage_stop1.wav",
            locked = "doors/door_metal_medium_open1.wav"
		},
        FlightLoop = "CoolyDude/silence_test_flight_new_seamless.wav",
        FlightLoopDamaged = "drmatt/tardis/flight_loop_damaged.wav",
    },
    ProjectedLight = {
        --color = Color(r,g,b), --Base color. Will use main interior light if not set.
        --warncolor = Color(r,g,b), --Warning color. Will use main interior warn color if not set.
        brightness = 0, --Light's brightness
        vertfov = 80,
        horizfov = 50, --vertical and horizontal field of view of the light. Will default to portal height and width.
        farz = 750, --FarZ property of the light. Determines how far the light projects.]]
        offset = Vector(-20.5, 0, 40), --Offset from box origin
        texture = "effects/flashlight/square" --Texture the projected light will use. You can get these from the Lamp tool.
    },
    Light={
        enabled=false,
    },
    Portal = {
        -- Generated by portals debug tool
        pos = Vector(35.4, 0, 50.2),
        ang = Angle(0, 0, 0),
        width = 55,
        height = 97,
        thickness = 54,
        inverted = 1,
    },
    Teleport = {
        SequenceSpeed = 0.65,
        SequenceSpeedWarning = 1,
        SequenceSpeedFast = 20,
        SequenceSpeedWarnFast = 10,
        DematSequence = {
            200,
            100,
            150,
            0
        },
        DematSequenceDelays={
            [1] = 5
        },
        MatSequence = {
            50,
            100,
            50,
            255
        },
        MatSequenceDelays={
            [2] = 0
        },
    },
}

T.CustomSettings = {
    interior_lightrotor = {
        text = "Rotor Colour",
        value_type = "list",
        value = "LRDefault",
        options = {
            ["LRDefault"] = "Default (Cold White)",
            ["LRBlue"] = "Blue",
            ["LRRed"] = "Red",
            ["LROrange"] = "Orange",
            ["LRGreen"] = "Green",
            ["LRPurple"] = "Purple",
            ["LRWarmWhite"] = "Warm White",
            ["LRPink"] = "Pink",
        }
    },
}

T.Templates = {

    silence_lightrotor_default = {
		override = true,
        condition = function(id, ply, ent)
            local setting_val = TARDIS:GetCustomSetting(id, "interior_lightrotor", ply)
            return (setting_val == "LRDefault")
        end,
	},

    silence_lightrotor_blue = {
		override = true,
        condition = function(id, ply, ent)
            local setting_val = TARDIS:GetCustomSetting(id, "interior_lightrotor", ply)
            return (setting_val == "LRBlue")
        end,
	},

    silence_lightrotor_red = {
		override = true,
        condition = function(id, ply, ent)
            local setting_val = TARDIS:GetCustomSetting(id, "interior_lightrotor", ply)
            return (setting_val == "LRRed")
        end,
	},

    silence_lightrotor_orange = {
		override = true,
        condition = function(id, ply, ent)
            local setting_val = TARDIS:GetCustomSetting(id, "interior_lightrotor", ply)
            return (setting_val == "LROrange")
        end,
	},

    silence_lightrotor_green = {
		override = true,
        condition = function(id, ply, ent)
            local setting_val = TARDIS:GetCustomSetting(id, "interior_lightrotor", ply)
            return (setting_val == "LRGreen")
        end,
	},

    silence_lightrotor_purple = {
		override = true,
        condition = function(id, ply, ent)
            local setting_val = TARDIS:GetCustomSetting(id, "interior_lightrotor", ply)
            return (setting_val == "LRPurple")
        end,
	},

    silence_lightrotor_warmwhite = {
		override = true,
        condition = function(id, ply, ent)
            local setting_val = TARDIS:GetCustomSetting(id, "interior_lightrotor", ply)
            return (setting_val == "LRWarmWhite")
        end,
	},

    silence_lightrotor_pink = {
		override = true,
        condition = function(id, ply, ent)
            local setting_val = TARDIS:GetCustomSetting(id, "interior_lightrotor", ply)
            return (setting_val == "LRPink")
        end,
	},

}

local function playerlookingat(self,ply,vec,fov,width)
    local disp = vec - self:WorldToLocal(ply:GetPos()+Vector(0,0,64))
    local dist = disp:Length()

    local maxcos = math.abs( math.cos( math.acos( dist / math.sqrt( dist * dist + width * width ) ) + fov * ( math.pi / 180 ) ) )
    disp:Normalize()

    if disp:Dot( ply:EyeAngles():Forward() ) > maxcos then
        return true
    end

    return false
end

T.Interior.CustomHooks = {
    use_console = {
        "Use",
        function(self,a,c)
            if SERVER and a:IsPlayer() and (not a:GetTardisData("outside")) and CurTime() > a:GetTardisData("outsidecool",0) then
                local pos=Vector(0,0,0)
                local pos2=self:WorldToLocal(a:GetPos())
                local distance=pos:Distance(pos2)
                if distance < 110 and playerlookingat(self,a,pos,10,10) then
                    TARDIS:Control("thirdperson_careful", a)
                end
            end
        end,
    },
}

local function is_selected_exterior(id, ply, ent, this_setting)
	if TARDIS:GetInterior(id).EnableClassicDoors ~= true then
		return false
	end

	local setting = TARDIS:GetCustomSetting(id, "exterior", ply)

	if setting ~= "random" then
		return (setting == this_setting)
	end

	local data = ent:GetData("hellbent_selected_exterior")
	if data ~= nil then
		return (this_setting == data)
	end
end


TARDIS:AddInterior(T)
