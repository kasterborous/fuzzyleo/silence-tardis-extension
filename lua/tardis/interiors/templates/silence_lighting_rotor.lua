TARDIS:AddInteriorTemplate("silence_lightrotor_default", {
	Interior = {

		Rotorcol = {
			color = Color(196, 218, 231),
		},

		Light = {
			color = Color(141, 211, 255),
			warncolor = Color(255, 60, 60),
			pos = Vector(0, 0, 100),
			brightness = 1.3,
		},
		Lights = {
			{
				color = Color(141, 211, 255),
				warncolor = Color(255, 60, 60),
				pos = Vector(0, 395, 80),
				brightness = 0.2,
				nopower = false
			},
			{
				color = Color(141, 211, 255),
				warncolor = Color(255, 60, 60),
				pos = Vector(0, 0, 100),
				brightness = 0.05,
				nopower = true
			},
		},

	},
})

TARDIS:AddInteriorTemplate("silence_lightrotor_blue", {
	Interior = {

		Rotorcol = {
			color = Color(62, 123, 255),
		},

		Light = {
			color = Color(23, 96, 255),
			warncolor = Color(255, 60, 60),
			pos = Vector(0, 0, 100),
			brightness = 1.3,s
		},
		Lights = {
			{
				color = Color(23, 96, 255),
				warncolor = Color(255, 60, 60),
				pos = Vector(0, 395, 80),
				brightness = 0.2,
				nopower = false
			},
			{
				color = Color(23, 96, 255),
				warncolor = Color(255, 60, 60),
				pos = Vector(0, 0, 100),
				brightness = 0.05,
				nopower = true
			},
		},

	},
})

TARDIS:AddInteriorTemplate("silence_lightrotor_red", {
	Interior = {

		Rotorcol = {
			color = Color(255, 23, 23),
		},

		Light = {
			color = Color(255, 23, 23),
			warncolor = Color(255, 23, 23),
			pos = Vector(0, 0, 100),
			brightness = 1.3,s
		},
		Lights = {
			{
				color = Color(255, 23, 23),
				warncolor = Color(255, 23, 23),
				pos = Vector(0, 395, 80),
				brightness = 0.2,
				nopower = false
			},
			{
				color = Color(255, 23, 23),
				warncolor = Color(255, 23, 23),
				pos = Vector(0, 0, 100),
				brightness = 0.05,
				nopower = true
			},
		},

	},
})

TARDIS:AddInteriorTemplate("silence_lightrotor_orange", {
	Interior = {

		Rotorcol = {
			color = Color(255, 150, 80),
		},

		Light = {
			color = Color(255, 150, 80),
			warncolor = Color(255, 60, 60),
			pos = Vector(0, 0, 100),
			brightness = 1.3,s
		},
		Lights = {
			{
				color = Color(255, 150, 80),
				warncolor = Color(255, 60, 60),
				pos = Vector(0, 395, 80),
				brightness = 0.2,
				nopower = false
			},
			{
				color = Color(255, 150, 80),
				warncolor = Color(255, 60, 60),
				pos = Vector(0, 0, 100),
				brightness = 0.05,
				nopower = true
			},
		},

	},
})

TARDIS:AddInteriorTemplate("silence_lightrotor_green", {
	Interior = {

		Rotorcol = {
			color = Color(156, 255, 169),
		},

		Light = {
			color = Color(80, 255, 103),
			warncolor = Color(255, 60, 60),
			pos = Vector(0, 0, 100),
			brightness = 1.3,
		},
		Lights = {
			{
				color = Color(80, 255, 103),
				warncolor = Color(255, 60, 60),
				pos = Vector(0, 395, 80),
				brightness = 0.2,
				nopower = false
			},
			{
				color = Color(80, 255, 103),
				warncolor = Color(255, 60, 60),
				pos = Vector(0, 0, 100),
				brightness = 0.05,
				nopower = true
			},
		},

	},
})

TARDIS:AddInteriorTemplate("silence_lightrotor_purple", {
	Interior = {

		Rotorcol = {
			color = Color(156, 80, 255),
		},

		Light = {
			color = Color(146, 63, 255),
			warncolor = Color(255, 60, 60),
			pos = Vector(0, 0, 100),
			brightness = 1.3,
		},
		Lights = {
			{
				color = Color(146, 63, 255),
				warncolor = Color(255, 60, 60),
				pos = Vector(0, 395, 80),
				brightness = 0.2,
				nopower = false
			},
			{
				color = Color(146, 63, 255),
				warncolor = Color(255, 60, 60),
				pos = Vector(0, 0, 100),
				brightness = 0.05,
				nopower = true
			},
		},

	},
})

TARDIS:AddInteriorTemplate("silence_lightrotor_warmwhite", {
	Interior = {

		Rotorcol = {
			color = Color(255, 237, 209),
		},

		Light = {
			color = Color(255, 247, 235),
			warncolor = Color(255, 60, 60),
			pos = Vector(0, 0, 100),
			brightness = 1.3,
		},
		Lights = {
			{
				color = Color(255, 247, 235),
				warncolor = Color(255, 60, 60),
				pos = Vector(0, 395, 80),
				brightness = 0.2,
				nopower = false
			},
			{
				color = Color(255, 247, 235),
				warncolor = Color(255, 60, 60),
				pos = Vector(0, 0, 100),
				brightness = 0.05,
				nopower = true
			},
		},

	},
})

TARDIS:AddInteriorTemplate("silence_lightrotor_pink", {
	Interior = {

		Rotorcol = {
			color = Color(243, 184, 255),
		},

		Light = {
			color = Color(232, 116, 255),
			warncolor = Color(255, 60, 60),
			pos = Vector(0, 0, 100),
			brightness = 1.3,
		},
		Lights = {
			{
				color = Color(232, 116, 255),
				warncolor = Color(255, 60, 60),
				pos = Vector(0, 395, 80),
				brightness = 0.2,
				nopower = false
			},
			{
				color = Color(232, 116, 255),
				warncolor = Color(255, 60, 60),
				pos = Vector(0, 0, 100),
				brightness = 0.05,
				nopower = true
			},
		},

	},
})