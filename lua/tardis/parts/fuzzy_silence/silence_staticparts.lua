-- Adds silence parts

local PART={}
PART.ID = "silence_rotor"
PART.Name = "silence_rotor"
PART.Model = "models/FuzzyLeo/Silence/timerotor.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_rotortop"
PART.Name = "silence_rotortop"
PART.Model = "models/FuzzyLeo/Silence/timerotortop.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_pillar"
PART.Name = "pillar"
PART.Model = "models/FuzzyLeo/Silence/pillar.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_pillar2"
PART.Name = "silence_pillar2"
PART.Model = "models/FuzzyLeo/Silence/pillarbig.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_floor"
PART.Name = "floor"
PART.Model = "models/FuzzyLeo/Silence/Floor.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_floorlight"
PART.Name = "silence_floorlight"
PART.Model = "models/FuzzyLeo/Silence/floorlight.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_doorframe"
PART.Name = "silence_doorframe"
PART.Model = "models/FuzzyLeo/Silence/doorframe.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_doorframelodge"
PART.Model = "models/FuzzyLeo/Silence/lodgerinteriordoorframe.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_doorframecl"
PART.Model = "models/FuzzyLeo/Silence/doorframeclassic.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_control"
PART.Name = "silence_control"
PART.Model = "models/FuzzyLeo/Silence/controls.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

-- Controls

local PART={}
PART.ID = "silence_cloak"
PART.Name = "silence_cloak"
PART.Model = "models/FuzzyLeo/Global/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_coords"
PART.Name = "silence_coords"
PART.Model = "models/FuzzyLeo/Global/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_demat"
PART.Name = "silence_demat"
PART.Model = "models/FuzzyLeo/Global/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_doorhitbox"
PART.Name = "silence_doorhitbox"
PART.Model = "models/FuzzyLeo/Global/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_doorhitbox2"
PART.Name = "silence_doorhitbox"
PART.Model = "models/FuzzyLeo/Global/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_doorlock"
PART.Name = "silence_doorlock"
PART.Model = "models/FuzzyLeo/Global/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_enginerelease"
PART.Name = "silence_enginerelease"
PART.Model = "models/FuzzyLeo/Global/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_fastreturn"
PART.Name = "silence_fastreturn"
PART.Model = "models/FuzzyLeo/Global/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_flight"
PART.Name = "silence_flight"
PART.Model = "models/FuzzyLeo/Global/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_float"
PART.Name = "silence_float"
PART.Model = "models/FuzzyLeo/Global/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_hads"
PART.Name = "silence_hads"
PART.Model = "models/FuzzyLeo/Global/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_handbrake"
PART.Name = "silence_handbrake"
PART.Model = "models/FuzzyLeo/Global/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_isomorphic"
PART.Name = "silence_isomorphic"
PART.Model = "models/FuzzyLeo/Global/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_manualcoords"
PART.Name = "silence_manualcoords"
PART.Model = "models/FuzzyLeo/Global/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_manualflight"
PART.Name = "silence_manualflight"
PART.Model = "models/FuzzyLeo/Global/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_physlock"
PART.Name = "silence_physlock"
PART.Model = "models/FuzzyLeo/Global/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_power"
PART.Name = "silence_power"
PART.Model = "models/FuzzyLeo/Global/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_radio"
PART.Name = "silence_radio"
PART.Model = "models/FuzzyLeo/Global/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_redecorate"
PART.Name = "silence_redecorate"
PART.Model = "models/FuzzyLeo/Global/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_repair"
PART.Name = "silence_repair"
PART.Model = "models/FuzzyLeo/Global/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_scanner"
PART.Name = "silence_scanner"
PART.Model = "models/FuzzyLeo/Global/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "silence_screen"
PART.Name = "silence_screen"
PART.Model = "models/FuzzyLeo/Global/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)